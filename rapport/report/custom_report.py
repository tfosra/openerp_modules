import time
from openerp.report import report_sxw

class rapport_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name,context=None):
        super(rapport_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
                 'time': time, 
                 })
    
#report_sxw.report_sxw('name of the report',  'class linked', 'path of the script', parser=thisClassASusual)
report_sxw.report_sxw('report.rpport.webkit', 'rapport.wizard', 'rapport/report/rapport_report.mako', parser=rapport_report)