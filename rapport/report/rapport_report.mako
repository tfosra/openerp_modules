<html>
<head>
    <style>
            ${css}
    </style>
</head>
<body>
	<h1>${_("LES CLASSES")} </h1>
	<table  class="basic_table">
	   <thead>
			<tr>
				<th class>${_("NOMS")}</th>
			</tr>
		</thead>
		%for nom in data['form']['classes']:
			<tbody>
				<tr>
					<td style="border-top:2px solid;"> ${nom} </td>  
				</tr>		
			</tbody>
		%endfor
    </table>
	
	${next.body()}
</body>
 </html>