from openerp.osv import osv

class rapport_wizard(osv.TransientModel):
    _name="rapport.wizard"
    
    def print_report(self,cr,uid, ids,context):
        res = {}
        res['classes'] = []
        cl_obj = self.pool.get('training.classroom')
        cl_ids = cl_obj.search(cr, uid, [])
        for cl in cl_obj.browse(cr, uid, cl_ids, context):
            res['classes'] += [cl.name]
        
        datas = {
                 'form' : res,
                 'params': {}
                }
        return {
               'type':'ir.actions.report.xml',
               'report_name':'rapport.webkit',
               'datas':datas,
               }

rapport_wizard()