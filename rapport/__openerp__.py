{
    "name": "Rapport",
    "version": "1.0",
    "depends": ["base", 'training'],
    "author": "Souleyman",
    "category": "TRAINING",
    'data': [
             'wizard/report_wizard.xml',
             'report/custom_report.xml',
             ],
    'installable': True,
    'active': True,
    'sequence': -25,
}