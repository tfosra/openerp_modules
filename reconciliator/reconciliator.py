# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class patch(osv.TransientModel):
    _name = 'test.reconciliator'
    
    _columns = {
            "move_line_id": fields.many2one('account.move.line', 'Move line'),
            "reconcile_id": fields.many2one('account.move.reconcile', 'Reconciliation'),
                    } 


    def do_action(self, cr, uid, ids, context={}):
        this = self.browse(cr, uid, ids[0], context)
        self.pool.get('account.move.line').write(cr, uid, this.move_line_id.id, {'reconcile_partial_id': this.reconcile_id.id})
        return True