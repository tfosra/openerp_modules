from openerp.osv import osv, fields

class classroom(osv.osv):
    _name = 'training.classroom'
    _columns = {
                'name': fields.char('Name', required=True),
                'description': fields.text('Description', translate=True),
                    }

class student(osv.osv):
    _name = 'training.student'
    
    def _get_student_from_mark(self, cr, uid, ids, context={}):
        res = {}
        for mark in self.pool.get('training.mark').browse(cr, uid, ids, context):
            res[mark.student_id.id] = True
        return res.keys()
    
    def _calc_moy(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for student in self.browse(cr, uid, ids, context):
            total = sum([m.total for m in student.mark_ids])
            res[student.id] = total / len(student.mark_ids) if student.mark_ids else 0.0
        return res
    
    _columns = {
                'name': fields.char('Name', required=True),
                'classroom_id': fields.many2one('training.classroom', 'Classroom'),
                'moy': fields.function(_calc_moy, string='Mark', readonly=True, store={
                                                                                       'training.mark': (_get_student_from_mark, ['total'], 10) 
                                                                                       }),
                'mark_ids': fields.one2many('training.mark', 'student_id', 'Marks'),
                    }

class mark(osv.osv):
    _name = 'training.mark'
    
    def _calc_total(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for mark in self.browse(cr, uid, ids, context):
            res[mark.id] = mark.value * mark.coeff
        return res
    
    def _calc_total_inv(self, cr, uid, ids, field_name, value, args, context={}):
        str_query = "update training_mark set total = %d where id=%s"%(value, ids)
        cr.execute(str_query)
        return True
    _columns = {
                'value': fields.float('Value', required=True),
                'coeff': fields.float('Coefficient', required=True),
                'total': fields.function(_calc_total, string='Total', fnct_inv=_calc_total_inv, readonly=True, store=True),
                'student_id': fields.many2one('training.student', 'Student', required=True),
                    }
    
    def write(self, cr, uid, ids, vals, context=None):
        vals.update({'total': 0.0})
        res = super(mark, self).write(cr, uid, ids, vals, context)
        return res 