{
    "name": "Training",
    "version": "1.0",
    "depends": ["base"],
    "author": "Souleyman",
    "category": "TRAINING",
    'data': [
             'security/training_security.xml',
             'security/ir.model.access.csv',
             'training_view.xml',
             'training_data.xml',
             ],
    'installable': True,
    'active': True,
    'sequence': -25,
}