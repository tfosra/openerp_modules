from openerp.osv import osv

class rapport_wizard(osv.TransientModel):
    _inherit="rapport.wizard"
    
    def print_report(self,cr,uid, ids,context):
        reponse = super(rapport_wizard, self).print_report(cr, uid, ids, context)
        res = reponse['datas']['form']
        res['student'] = []
        st_obj = self.pool.get('training.student')
        st_ids = st_obj.search(cr, uid, [])
        for st in st_obj.browse(cr, uid, st_ids, context):
            res['student'] += [st.name]
        
        return reponse

rapport_wizard()