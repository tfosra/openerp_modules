{
    "name": "Rapport - Inherited",
    "version": "1.0",
    "depends": ["base", 'rapport'],
    "author": "Souleyman",
    "category": "TRAINING",
    'data': [
             'report/custom_report.xml',
             ],
    'installable': True,
    'active': True,
    'sequence': -25,
}