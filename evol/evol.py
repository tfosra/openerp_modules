from openerp.osv import osv, fields

class song(osv.osv):
    _name = 'evol.song'
    _columns = {
            'name': fields.char('Title', required=True),
            'property_evol_album': fields.property('evol.album',
                                        type='many2one',
                                        relation='evol.album',
                                        string='Album',
                                        view_load=True,
                                        group_name='Album definition',
                                        )
                    }

class album(osv.osv):
    _name = 'evol.album'
    _columns = {
            'name': fields.char('Label', required=True),
                    }

class dummy_class(osv.osv):
    _name = 'dummy.class'