{
    "name": "evol",
    "version": "1.0",
    "depends": ["base"],
    "author": "Souleyman",
    "category": "EVOL",
    "init_xml": [],
    'data': [
             'security/evol_security.xml',
             'security/ir.model.access.csv',
             'evol_view.xml',
             ],
    'update_xml': [],
    'demo_xml': [],
    'installable': True,
    'active': False,
    'sequence': -1,
}